﻿using UnityEngine;
using System.Collections;

public class ScrollUV : MonoBehaviour {

	private Vector2 prevPos;

	// Update is called once per frame
	void Update () {
		MeshRenderer mr = GetComponent<MeshRenderer> (); 
		Material mat = mr.material;

		Vector2 offset = mat.mainTextureOffset;

		// for slower transform
//		offset.y = transform.position.y /transform.localScale.y;
		offset.y = transform.position.y;

		mat.mainTextureOffset = offset;
	}
}
