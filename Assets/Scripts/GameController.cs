﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


[System.Serializable]
public class DefineRange
{
    public float min, max;
}


public class GameController : MonoBehaviour {

    public GameObject rightHazard;
    public GameObject leftHazard;
    public Vector2 spawnValue;
    public int hazardCount; // repeat creating hazard
	public int maxObstaclePerSpawn = 5;

    public float spawnWait; // time for each spawn obstacle
    public float waveWait; // time for each wave 
	public float minPositionFromPlayer=5;
	public float maxPositionFromPlayer=10;

	public Text scoreText;
	public int score;

	private GameObject player;

	void Start () {
		score = 0;
		UpdateScore ();
		player = GameObject.FindGameObjectWithTag ("Player");
        StartCoroutine(SpanWaves());
	}

	void FixedUpdate(){
		if (player != null) {
			score = (int)Mathf.Ceil (player.transform.position.y);
			UpdateScore ();
		}
	}

	float GetRange(){
		if (player != null) {
			return Random.Range (player.transform.position.y + minPositionFromPlayer, player.transform.position.y + maxPositionFromPlayer);
		}
		return 0f;
	}

    IEnumerator SpanWaves()
    {
		yield return new WaitForSeconds(waveWait);
        while (true && player != null)
        {
            for (int i = 0; i < hazardCount && player != null; i++)
            {
				// spawn enemies
				for (int j=0; j < Random.Range (1, maxObstaclePerSpawn); j++){
					if ( j % 2 == 0){
						Vector2 spawnPosition = new Vector2(spawnValue.x * -1, GetRange() );
	                	Instantiate(leftHazard, spawnPosition, Quaternion.identity);
					} else {
						Vector2 spawnPosition2 = new Vector2(spawnValue.x, GetRange () );
	                	Instantiate(rightHazard, spawnPosition2, Quaternion.identity);
					}
				}
                yield return new WaitForSeconds(spawnWait);
            }
        }
    }

	void UpdateScore(){
		scoreText.text = score +" m";
	}
}
