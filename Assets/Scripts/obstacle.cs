﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {
	// default values
    public float speed = 5; 

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().velocity = Vector2.left * randomspeed(speed);
	}

    float randomspeed(float sp)
    {
        return sp + -Random.Range(-2, 2);
    }
}
