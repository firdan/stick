﻿using UnityEngine;
using System.Collections;

public class PlayerScripts : MonoBehaviour {

	public float force = 10;
    public AudioClip impact;
    public AudioSource crashSound;
	public ParticleSystem flame;
	public GameObject GameOverPanel;


	private Animator animator;

	// Update is called once per frame
    void Start()
    {
        crashSound = GetComponent<AudioSource>();
		animator = GetComponent<Animator> ();
		GameOverPanel.SetActive (false);
    }

	void FixedUpdate () {
		bool flameOn = Input.anyKey;
		if (flameOn) {
			GetComponent<Rigidbody2D> ().AddForce (Vector2.up * force);
		}
		AdjustFlame (flameOn);
	}

	void AdjustFlame (bool flameActive)
	{
		flame.enableEmission = flameActive;
		flame.emissionRate = flameActive? 300.0f : 75.0f; 
	}

	void OnCollisionEnter2D(Collision2D coll) {	
		// Restart
		if (coll.gameObject.tag == "Obstacle") {
			// do something
//			Time.timeScale = 0;
			animator.enabled = true;
			animator.SetBool("dead",true);

			GameOverPanel.SetActive(true);
			GameOverPanel.GetComponent<Animator>().enabled = true;

//            crashSound.PlayOneShot(impact,1.0f);
//            Application.LoadLevel("gameover");

			// restart the level
			//Application.LoadLevel (Application.loadedLevel);
		}
	}
}
