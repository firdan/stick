﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour {
    
    public int goodJob; // limit to display badge on goodjob
    public int booJob; // limit to display badge on badjob
	public Image badge;
	public Sprite goodjobBadge;
	public Sprite boojobBadge;
	public AudioSource goodjobAudio;
	public AudioSource boojobAudio;
	public AudioSource normalAudio;

	public Text scoreText;
    public Text hiscoreText;
	public GameObject gameController;


	void Start(){
		// remove the sprite
		badge.enabled = false;
	}

	void SetGoodjobBadge(){
		badge.enabled = true;
		badge.sprite = goodjobBadge;
		goodjobAudio.enabled = true;
	}

	void SetBoojobBadge(){
		badge.enabled = true;
		badge.sprite = boojobBadge;
		boojobAudio.enabled = true;
	}

	void SetBadge(int score)
	{
		if (score > goodJob) {
			SetGoodjobBadge (); 
		} else if (score < booJob) {
			SetBoojobBadge ();
		} else {
			// no badge, let's play some sfx
			normalAudio.enabled = true;
		}
	}

	void SetScore(){

		GameObject mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");

		mainCamera.GetComponent<AudioSource> ().Pause ();

		int hiscore = PlayerPrefs.GetInt("hiscore");
		int score = gameController.GetComponent<GameController> ().score;

		// set current score
		scoreText.text = score.ToString () + " m";

		// set high score
		if (hiscore < score ) {
			hiscore = score;
			PlayerPrefs.SetInt("hiscore", score);
		}

		hiscoreText.text = hiscore.ToString ()+ " m";
		SetBadge (score);
	}

	void FixedUpdate(){
		// update current score
		SetScore ();
	}
}
