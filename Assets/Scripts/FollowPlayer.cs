﻿using UnityEngine;
using System.Collections;

public class FollowPlayer: MonoBehaviour {

	public Transform target;
	public Vector3 offset;

	void LateUpdate () {
		if (target != null) {
			transform.position = new Vector3 (transform.position.x + offset.x,
		                                 target.position.y + offset.y,
		                                 transform.position.z + offset.x);
		}
	}

}
