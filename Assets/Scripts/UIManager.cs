﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {

	public GameObject player;
	public GameObject gameController;
	public Animator title;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void HideTitleOnPlayerMove(){
		if (player.transform.position.y > 0) {
			// run the animation
			title.enabled=true;
		}
	}

	public void RestartLevel(){
		Application.LoadLevel (Application.loadedLevel);
	}

	void FixedUpdate(){
		if (player != null) {
			HideTitleOnPlayerMove ();
		}
	}
}
