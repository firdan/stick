﻿using UnityEngine;
using System.Collections;

public class ScrollerBackground : MonoBehaviour {

	public float scrollSpeed;
	public float tileSizeY;
	
	private Vector2 savedOffset;
	private Vector3 startPosition;
	
	void Start ()
	{
		Debug.Log ("start position " +transform.position.ToString ("F4"));
		startPosition = transform.position;
		savedOffset =  GetComponent<Renderer>().sharedMaterial.GetTextureOffset ("_MainTex");
	}
	
	void Update ()
	{
		float z = Mathf.Repeat (Time.time * scrollSpeed, tileSizeY * 4);
		z = z / tileSizeY;
		z = Mathf.Floor (z);
		z = z / 4;
		Vector2 offset = new Vector2 (z, savedOffset.y);
		GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", offset);
		float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeY);
		Debug.Log (newPosition.ToString("F4"));
		transform.position = startPosition + Vector3.down * newPosition;
	}
	
	void OnDisable () {
		GetComponent<Renderer>().sharedMaterial.SetTextureOffset ("_MainTex", savedOffset);
	}
}
