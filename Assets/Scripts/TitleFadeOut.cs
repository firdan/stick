﻿using UnityEngine;
using System.Collections;

public class TitleFadeOut : MonoBehaviour
{
    public float minimum = 0.0f;
    public float maximum = 1f;
    public float duration = 5.0f;
    private float startTime;
    public SpriteRenderer sprite;
    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        if (Input.anyKey)
        {
            //
            //GetComponent<SpriteRenderer>().color.a = Mathf.Lerp(GetComponent<SpriteRenderer>().color.a, 0, Time.deltaTime * fadespeed);
            float t = (Time.time - startTime) / duration;
            sprite.color = new Color(1f, 1f, 1f, Mathf.SmoothStep(minimum, maximum, t));
        }
    }
}
