﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour {

	public float scrollSpeed;
	public float tileSizeZ;
	
	private Vector2 startPosition;
	
	void Start () 
	{
		startPosition = transform.position;
	}
	
	void Update ()
	{
		float newPosition = Mathf.Repeat (Time.time * scrollSpeed, tileSizeZ * 2);
		transform.position = startPosition + Vector2.down * newPosition;
	}
}
